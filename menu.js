$(function () {
  if ($("body").hasClass("hacked")) {
    return;
  }
  else {
    $("body").addClass("hacked");
  }
  let newLink = $("<a>");
  newLink.attr("href", "https://exercism.io/mentor/dashboard/next_solutions");
  newLink.text("New solutions to mentor");
  $("a[href='/mentor/dashboard'").parent().after($("<li>").append(newLink));
});
