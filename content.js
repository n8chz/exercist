$(function () {
    $(".solution").each(function () {
        let that = $(this);
        $.get({
            url: `https://exercism.io${$(this).attr("href")}`,
            success: function (page) {
              let doc = (new DOMParser()).parseFromString(page, "text/html");
              let bytes = $(".solution-code", doc).text().length;
              let msg = ` ${bytes} bytes `;
              bar = that.find(".details-bar");
              bar.append($("<div>").addClass("detail").text(msg));
            }
        });
    });
});
